import {START_CLOCK, STOP_CLOCK, RESET, LAP, UPDATE} from './action/types'
import produce from 'immer';

const initialState = {
    start: false,
    stateTime: 0,
    time: {
        hrs: 0,
        min: 0,
        second: 0,
        milli: 0
    },
    lastLap: 0,
    lap: [],
    freezeTime: [],
}

const reducer = (state=initialState, action) => {
    return produce(state, draft => {
        switch (action.type) {
            case START_CLOCK :
                if(!draft.start){
                    draft.stateTime = Math.floor(Date.now()/10) -
                    (draft.time.hrs*360000+draft.time.min*6000
                        + draft.time.second*100+ draft.time.milli)
                }
                draft.lastLap = draft.stateTime + draft.lastLap
                draft.start = true
                break

            case STOP_CLOCK :
                draft.start = false
                draft.lastLap = draft.lastLap - draft.stateTime
                break

            case RESET :
                const startTime = {
                    hrs: 0,
                    min: 0,
                    second: 0,
                    milli: 0
                }
                draft.start = false
                draft.stateTime = 0
                draft.freezeTime = []
                draft.lastLap = 0
                draft.lap = []
                draft.time = startTime
                break
            case LAP :
                const timeInMilli = Math.floor(Date.now()/10)
                let lapTime = 0
                if(draft.lastLap !== 0){
                    lapTime = timeInMilli - draft.lastLap
                }else{
                    lapTime = timeInMilli - draft.stateTime
                }
                const newTime = calTimeFromMIlli(lapTime)
                draft.lastLap = timeInMilli
                draft.freezeTime.push(draft.time)
                draft.lap.push(newTime)
                break
            case UPDATE :
                const latestTime = Math.floor(Date.now()/10)
                const updatedTime = calTimeFromMIlli(latestTime - draft.stateTime)
                draft.time = updatedTime
                break
            default: 
                return draft
        }
    })
}

const calTimeFromMIlli = (lapTime) => {
    let lapHrs = 0
    let lapMin = 0
    let lapsec = 0
    let lapMilli = 0
    if (lapTime >= 360000 ){
        lapHrs = Math.floor(lapTime/360000)
        lapTime = lapTime % 360000
        if(lapTime >= 6000){
            lapMin = Math.floor(lapTime/6000)
            lapTime = lapTime % 6000
            if(lapTime >= 100){
                lapsec = Math.floor(lapTime/100)
                lapMilli = lapTime % 100  
            }else{
                lapsec = 0
                lapMilli = lapTime
            }
        }else{
            lapMin = 0
            if(lapTime >= 100){
                lapsec = Math.floor(lapTime/100)
                lapMilli = lapTime % 100  
            }else{
                lapsec = 0
                lapMilli = lapTime
            }
        }
    }else{
        lapHrs = 0
        if(lapTime >= 6000){
            lapMin = Math.floor(lapTime/6000)
            lapTime = lapTime % 6000
            if(lapTime >= 100){
                lapsec = Math.floor(lapTime/100)
                lapMilli = lapTime % 100  
            }else{
                lapsec = 0
                lapMilli = lapTime
            }
        }else{
            lapMin = 0
            if(lapTime >= 100){
                lapsec = Math.floor(lapTime/100)
                lapMilli = lapTime % 100  
            }else{
                lapsec = 0
                lapMilli = lapTime
            }
        }
    }
    return {
        hrs: lapHrs,
        min: lapMin,
        second: lapsec,
        milli: lapMilli,
    }
}

export default reducer
import { createStore, combineReducers } from 'redux';

import ClockReducer from './clockReducer'

const rootReducer = combineReducers({
    clock: ClockReducer
})
const store = createStore(rootReducer)

export default store
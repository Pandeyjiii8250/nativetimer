
import {START_CLOCK, STOP_CLOCK, RESET, LAP, UPDATE} from './types';

export const startClock = () => {
    return {
        type: START_CLOCK
    }
}

export const stopClock = () => {
    return {
        type: STOP_CLOCK
    }
}

export const reset = () => {
    
    return {
        type: RESET
    }
}

export const lap = () => {
    return {
        type: LAP
    }
}

export const update = () => {
    return {
        type: UPDATE
    }
}

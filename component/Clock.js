import React, {useEffect} from 'react'
import {StyleSheet, View, Text} from 'react-native'

import { useSelector, useDispatch } from 'react-redux'
import { update } from '../store/action/clockActionCreator'
const Clock = () => {

    const start = useSelector(state => state.clock.start)
    const time = useSelector(state => state.clock.time)
    const dispatch = useDispatch()

    useEffect(() => {
        let timerId
        if (start) {
            timerId = setInterval(()=>{
                dispatch(update())
            }, 10)
        }
        return () => {
            clearInterval(timerId)
        }
    }, [start])

    return (
        <View style={styles.dispClock}>
            <Text style={styles.timeText}>{time.hrs<10 ? ` 0${time.hrs}` : time.hrs}</Text>
            <Text style={styles.timeText}>:</Text>
            <Text style={styles.timeText}>{time.min<10 ? ` 0${time.min}` : time.min }</Text>
            <Text style={styles.timeText}>:</Text>
            <Text style={styles.timeText}>{time.second < 10 ? ` 0${time.second}` : time.second}</Text>
            <Text style={styles.timeText}>.</Text>
            <Text style={styles.timeText}>{time.milli < 10 ? ` 0${time.milli}` : time.milli}</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    dispClock : {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center'
    },
    timeText : {
        fontSize: 50,
        fontWeight: 'bold'
    },
})

export default Clock
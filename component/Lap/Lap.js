import React from 'react'
import { View, Text, StyleSheet } from 'react-native'

import {useSelector } from 'react-redux'
import Row from './Row'

export default function Lap() {
    const laps = useSelector(state => state.clock.lap)
    const lapTime = useSelector(state => state.clock.freezeTime)

    return ( 
        <View style={styles.table}>``
            <View style={styles.tableHead}>
                <Text  style={styles.tableHeadText}> Sr. No.</Text>
                <Text style={styles.tableHeadText}> lap</Text>
                <Text style={styles.tableHeadText}>Last Lap</Text>
            </View>
            {laps.map((lap, index) => {
                console.log(lap)
                return <Row index={index} lap={lap} lapTime={lapTime[index]}/>
            })}
        </View>
    )
}


const styles = StyleSheet.create({
    table : {
        width: 500,
    },
    tableHead : {
        flex: 1,
        flexDirection: 'row',
        // backgroundColor: 'yellow',
        justifyContent: 'space-evenly'
    },
    tableHeadText : {
        
        fontSize: 24,
        fontWeight: 'bold',
        alignSelf: 'center'
    }
})
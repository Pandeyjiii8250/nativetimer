import React from 'react'
import { View, Text, StyleSheet, ScrollView } from 'react-native'


export default function Row({index, lap, lapTime}) {
    return (
        <ScrollView>
            <View style={styles.row}>
                <Text>{index + 1}</Text>
                <View style={styles.cell}>
                    <Text>{lap.hrs < 10 ? ` 0${lap.hrs}` : lap.hrs}</Text>
                    <Text>:</Text>
                    <Text>{lap.min < 10 ? `0${lap.min}` : lap.min}</Text>
                    <Text>:</Text>
                    <Text>{lap.second < 10 ? `0${lap.second}` : lap.second}</Text>
                    <Text>.</Text>
                    <Text>{lap.milli < 10 ? ` 0${lap.milli}`: lap.milli}</Text>
                </View>
                <View style={styles.cell}>
                    <Text>{lapTime.hrs < 10 ? ` 0${lapTime.hrs}` : lapTime.hrs}</Text>
                    <Text>:</Text>
                    <Text>{lapTime.min < 10 ? `0${lapTime.min}` : lapTime.min}</Text>
                    <Text>:</Text>
                    <Text>{lapTime.second < 10 ? `0${lapTime.second}` : lapTime.second}</Text>
                </View>
            </View>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    row: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        marginTop: 20
    },
    cell : {
        flexDirection: 'row'
    }
})
import React from 'react'
import { MaterialIcons } from '@expo/vector-icons';
import { TouchableWithoutFeedback } from 'react-native';

import { useDispatch } from 'react-redux';
import { lap } from '../../store/action/clockActionCreator';

export const LapBtn = () => {

    const dispatch = useDispatch()

    const handleLap = () => {
        dispatch(lap())
    }
    return (
        <TouchableWithoutFeedback onPress={handleLap}>
            <MaterialIcons name="outlined-flag" size={30} color="steelblue" />
        </TouchableWithoutFeedback>
    )
}

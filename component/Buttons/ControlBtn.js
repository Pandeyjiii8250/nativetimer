import React  from 'react';
import { MaterialIcons } from '@expo/vector-icons';
import { TouchableWithoutFeedback } from 'react-native';

import { useSelector, useDispatch } from 'react-redux'
import { stopClock, startClock } from '../../store/action/clockActionCreator';

export const ControlBtn = () => {
    // const [play, updatePlay] = useState(false)
    const start = useSelector((state) => state.clock.start)
    const dispatch = useDispatch()

   
    const handleClick = () => {
        if (start) {
            dispatch( stopClock() )
        }else {
            dispatch (startClock())
        }
    }
    return (
        <TouchableWithoutFeedback onPress = {handleClick}>
            {start ?
            <MaterialIcons  name="pause" size={30} color="steelblue" /> :
            <MaterialIcons  name="play-arrow" size={30} color="steelblue" /> 
            }
        </TouchableWithoutFeedback>
    )
}

import React from 'react'
import { MaterialIcons } from '@expo/vector-icons';
import { TouchableWithoutFeedback } from 'react-native';

import {useSelector, useDispatch } from 'react-redux'
import { reset } from '../../store/action/clockActionCreator';

export const ResetBtn = () => {
    const dispatch = useDispatch()
    
    const handleReset = () => {
        dispatch(reset())
    }
    return (
        <TouchableWithoutFeedback onPress = {handleReset}>
            <MaterialIcons name="refresh" size={30} color="steelblue" />
        </TouchableWithoutFeedback>
    )
}

export default ResetBtn

import React from 'react';
import { StyleSheet, View } from 'react-native';

import Clock from './component/Clock';
import { ControlBtn } from './component/Buttons/ControlBtn';
import { LapBtn } from './component/Buttons/LapBtn';
import { ResetBtn } from './component/Buttons/ResetBtn'
import { Provider } from 'react-redux';
import  Lap from './component/Lap/Lap'
import store from './store/store'

export default function App() {
  
  return (
    <Provider store={store}>
      <View style={styles.container}>
          <Clock />
          <Lap />
          <View style={styles.btnContainer}>
            <ControlBtn />
            <LapBtn />
            <ResetBtn />
        </View>
        {/* <StatusBar style="auto" /> */}
      </View>
    </Provider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnContainer : {
    flexDirection: 'row',
    width: 150,
    justifyContent: 'space-evenly',
    paddingTop: 20,
    paddingBottom: 20,

  }
});
